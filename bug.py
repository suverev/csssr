#!/usr/bin/env python
# -*- coding: utf-8 -*-
from selenium import webdriver

softURL = "http://monosnap.com/"
driver = webdriver.Chrome()
driver.get("http://blog.csssr.ru/qa-engineer/")

elem = driver.find_element_by_xpath('//div[@class="graphs-errors"]/a')
elem.click()
elem = driver.find_element_by_xpath('//a[./text()="Софт для быстрого создания скриншотов"]')
elem.click()
currentURL = driver.current_url
try:
    assert softURL in currentURL
except AssertionError:
    print('String %s is not equal %s') % (currentURL, softURL)
driver.close()
